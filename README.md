# Directional Indicator For Hololens

A unity package that provides the functionality for implementing directional indicator for Hololens.

## Steps to use
1. Start a new project and create a new scene with the Main Camera and Directional light.
2. Create a new tag in the Tags menu named 'Target'.
3. Create a new empty gameobject in scene, name it as 'Cursor' also select an icon for it.
4. Create a new empty gameobject and name it as 'DirectionalIndicatorManager' and add
	* DirectionalIndicator script -> Assign value of cursor by draging and dropping 'Cursor' from hierarchy  -> Set values of other slidable fields.
	* ArrowObjectPool script -> Assign pooled object value from prebabs folder by dragging and dropping Directional indicator prefab.
	* BoxObjectPool script -> Assign pooled object value from prebabs folder by dragging and dropping Ring prefab.
5. You can create your own prefabs that were used above
	- create new models for both kind of indicators and import them in unity
	- drag the model in scene view.
	- add 'Indicator' script and 'OverlayNoZTest' Material.
	- make sure there are no colliders and rigidbody componets on the objects.
	- drag and drop the object in Prefabs folder and delete it from hierarchy.
	- Do it for both indicator models.
6. Add some targets obejects in scene and add 'Target' script to them and change the values in script accordingly.
7. Add 'MouseLook' script to main camera(to test in editor only).
8. Save scene and Play.
