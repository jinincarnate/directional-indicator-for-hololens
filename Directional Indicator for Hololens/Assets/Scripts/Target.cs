﻿using UnityEngine;

/// <summary>
/// Attach this script to the targets in your scene.
/// </summary>
public class Target : MonoBehaviour
{
    [SerializeField] private Color targetColor;
    [SerializeField] private bool needBoxIndicator = true;
    [SerializeField] private bool needArrowIndicator = true;

    private string targetTag = "Target";

    /// <summary>
    /// Gets the color of indicator for the given target.
    /// </summary>
    public Color TargetColor
    {
        get
        {
            return targetColor;
        }
    }

    /// <summary>
    /// Gets if the box indicator is required for the given target.
    /// </summary>
    public bool NeedBoxIndicator
    {
        get
        {
            return needBoxIndicator;
        }
    }

    /// <summary>
    /// Gets if the arrow indicator is required for the given target.
    /// </summary>
    public bool NeedArrowIndicator
    {
        get
        {
            return needArrowIndicator;
        }
    }

    private void Awake()
    {
        gameObject.tag = targetTag; //Set the tag of the target.
    }
}
